#ifndef PKG_GNUNET_H__
#define PKG_GNUNET_H__ 1

#include "driver.h"

#ifdef USE_GNUNET
# ifdef HAS_GNUNET

#include <gnunet/gnunet_cadet_service.h>

#include "typedefs.h"

/* --- Types --- */

/* --- Macros --- */

/* --- Variables --- */

/* --- Prototypes --- */

extern void gnunet_global_init(void);
extern void gnunet_global_deinit(void);

extern int cadet_read(interactive_t *ip, char *buffer, int length);
extern int cadet_write(interactive_t *ip, char *buffer, int length);
extern void cadet_deinit_connection (interactive_t *ip);

/* --- Efun Prototypes --- */

extern svalue_t *v_cadet_init_connection(svalue_t *sp, int num_arg);
extern svalue_t *f_cadet_deinit_connection(svalue_t *sp);

#  endif /* HAS_GNUNET */
# endif /* USE_GNUNET */
#endif /* PKG_GNUNET_H__ */

