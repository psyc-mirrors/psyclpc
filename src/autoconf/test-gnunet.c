#include <gnunet/platform.h>
#include <gnunet/gnunet_crypto_lib.h>
#include <gnunet/gnunet_common.h>
#include <gnunet/gnunet_util_lib.h>
#include <gnunet/gnunet_cadet_service.h>

int main(void) {
    const struct GNUNET_HashCode *hash = GC_u2h (4404);
    puts( GNUNET_h2s_full( hash ) );
    return 0;
}

