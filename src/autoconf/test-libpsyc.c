#include <stdio.h>
#include <psyc.h>

int main(void) {
    size_t tlen = 0;
    const char *t = psyc_template(PSYC_MC_REQUEST_CONTEXT_ENTER, &tlen);
    puts(t);
    return PSYC_VERSION ? 0 : -1;
}

